FROM ubuntu:17.04

RUN apt-get update && \
    apt-get install -y  wget

ADD http://www.perforce.com/downloads/perforce/r17.3/bin.linux26x86_64/p4v.tgz /tmp

RUN cd /tmp &&\
    tar zxf p4v.tgz &&\
    mv $(find . -maxdepth 1 -type d | grep ./) /p4merge
#RUN wget -O /tmp/p4v.tgz www.perforce.com/downloads/perforce/r17.3/bin.linux26x86_64/p4v.tgz &&\
#    cd /tmp &&\
#    tar zxf /tmp/p4v.tgz


CMD ["/p4merge/bin/p4merge"]
